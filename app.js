const express = require("express");
const {ApolloServer, gql} = require("apollo-server-express");


const app = express()

const typeDefs = gql`
    type Query{
        greetings: String!
    }
`
const resolvers = {
    Query: {
        greetings: () => 'hello'
    }
};


const apolloServer = new ApolloServer({
    typeDefs,
    resolvers
});

apolloServer.applyMiddleware({
    app, path: '/graphql'
})
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`App Listening on port ${PORT}`)
})

